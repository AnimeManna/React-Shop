import React, { Component } from 'react';
import { connect } from 'react-redux'
import './App.css';
import Header from './components/Header/Header'
import Content from './components/Content/Content'
import {getTracks} from './actions/tracks'


class App extends Component {



  addName(){
    console.log('userName',this.nameInput.value);
    this.props.onAddName(this.nameInput.value);
    this.nameInput.value = '';
  }


  render() {
    return (
      <div className="App" >
        <input type="text" placeholder="Enter name" ref={(input)=>{this.nameInput = input}} />
          <button onClick={this.addName.bind(this)}>Save</button>
          <ul>
              {this.props.user.map((user, index) =>
                  <li key={index}>{user}</li>
              )}
          </ul>
          <Header />
          <Content/>
          <div>
              <button onClick={this.props.onGetTracks}>
                  Get Tracks
              </button>
              <ul>
                  {this.props.tracks.map((track,index) =>
                      <li key={index}>{track.name}</li>
                  )}
              </ul>
          </div>
        ololo
      </div>
    );
  }
}

export default connect(
    state => ({
        user: state.user,
        tracks:state.tracks
    }),
    dispatch => ({
        onAddName: (userName) => {
            dispatch({ type: 'ADD_USER', payload: userName });
        },
        onGetTracks: ()=>{
            dispatch(getTracks())
        }
    })
)(App);
