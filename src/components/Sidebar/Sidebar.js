import React,{Component} from 'react'
import axios from 'axios'



export default class Sidebar extends Component{

    state = {
        name: ''
    };

    handleChange = event => {
        this.setState({ name: event.target.value });
    }

    handleSubmit = event => {
        event.preventDefault();

        const user = {
            name: this.state.name
        };

        axios.post(`https://jsonplaceholder.typicode.com/users`, { user })
            .then(res => {
                console.log('axios',res);
                console.log(res.data);
            })
    }

    render(){
        return(
            <div>
                <form onSubmit = {this.handleSubmit}>
                    <label>
                        <input type="text" onChange={this.handleChange} />
                    </label>
                    <button type="submit">Add</button>
                </form>
                Sidebar here
            </div>
        )
    }
}