import React, { Component } from 'react'
import { connect } from 'react-redux'

class Header extends Component{

    addToggleSidebar(){
        this.props.toggleSidebar();
    }

    render(){
        return(
            <div className="Header">
                Header is this
                <button onClick={()=>{
                    this.addToggleSidebar()
                }}>Go boy</button>
            </div>
        )
    }
}
export default connect(
    store=>({
        state:store
    }),
    dispatch=>({
        toggleSidebar(){
            dispatch({type:'TOGGLE_SIDEBAR'})
        }

    })
)(Header);