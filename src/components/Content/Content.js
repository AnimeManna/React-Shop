import React,{Component} from 'react';
import Sidebar from '../Sidebar/Sidebar';
import { connect } from 'react-redux';
import axios from 'axios'

class Content extends Component{

    state = {
        person:[]
    };

    componentDidMount(){
        axios.get('https://jsonplaceholder.typicode.com/users')
            .then(res=>{
                this.setState({person: res.data})
            })
            .catch(
                ()=>{
                    console.log('Error');
                }
            )
    }

    render(){
        console.log(this.props.sidebar.sidebarIsActive)
        return(
            <div>
                Content here
                <div>
                    {this.props.sidebar.sidebarIsActive
                        ? <Sidebar />
                        : null}
                </div>
                <ul>
                    {this.state.person.map((person)=> {
                      return  <li key={person.id}>{person.name}</li>
                    })}
                </ul>
            </div>
        )
    }
}
export default connect(
    state => ({
        sidebar:state.sidReducer,
    }),
    null
)(Content)