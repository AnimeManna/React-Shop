const initialState = {
    sidebarIsActive : true
};

export default function sidReducer ( state = initialState,action) {

    switch(action.type){
        case 'TOGGLE_SIDEBAR':{
            const newState={
                sidebarIsActive:!state.sidebarIsActive
            };
            return{
                ...state,
                ...newState
            };
        }
        default:
        return state
    }

    /*if(action.type==='TOGGLE_SIDEBAR'){
        const newState={
            sidebarIsActive:!state.sidebarIsActive
        };
        return{
            ...state,
            ...newState
        }
    }
    return state*/

}