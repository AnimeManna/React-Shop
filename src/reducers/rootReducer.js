import { combineReducers } from 'redux'
import page from './page'
import user from './user'
import tracks from './tracks'
import sidReducer from './sidReducer'

export default combineReducers({
    page,
    tracks,
    sidReducer,
    user
});